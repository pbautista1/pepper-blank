%dw 1.0
%output application/xml skipNullOn="everywhere"
%var ctr = 0
%var genAddId = now as :number {unit: "milliseconds"}
---
{
Package : {
   Content : {
      Application @(UniqueID : payload.Application.Identifier) : {
		  SalesChannel @(Type: payload.Application.@SalesChannelType): {
		  		  Company @(CompanyName : payload.Application.SalesChannel.CompanyName.@BusinessName,
				            LicenceNumber : payload.Application.SalesChannel.CompanyName.@LicenceNumber): {},
			   LoanWriter @(UniqueID : payload.Application.SalesChannel.Identifier, 
			   	            NameTitle : payload.Application.SalesChannel.PersonName.NameTitle.@Value,
			   	            FirstName : payload.Application.SalesChannel.PersonName.FirstName,			   	           				  
                            OtherName : payload.Application.SalesChannel.PersonName.OtherName,
                            Surname : payload.Application.SalesChannel.PersonName.Surname
			   )
			   : {			       
				  Contact @(x_Adress : genAddId,
				  	        Email : payload.Application.SalesChannel.Email
				  ) : {				      
					  MobileNo @(Number : payload.Application.SalesChannel.WorkPhone.Phone.Mobile) : {},
					  OfficePhone @(Number : payload.Application.SalesChannel.WorkPhone.Phone.FixedPhone,
					  	            AustralianDialingCode : payload.Application.SalesChannel.WorkPhone.Phone.FixedPhone.@AreaCode,
					  	            CountryCode : payload.Application.SalesChannel.WorkPhone.FixedPhone.@CountryCode): {}					  
				  }
			   }
		  },
		  (payload.Application.PartySegment.*Party map {
		        (CompanyApplicant @(UniqueID : $.Identifier,
		        	                Category : "Applicant",
                                    PartyType: "Company",
                                    Role : $.@Type,
                                    PrimaryApplicant : $.@PrimaryApplicant, 
                                    IsExistingCustomer : $.Privacy.@ExistingCustomer,
                                    AllowDirectMarketing : $.Privacy.@ExistingCustomer,
                                    BusinessName : $.Company.CompanyName.@BusinessName,
                                    DateRegistered : $.Company.RegisteredIn.Date,
                                    RegisteredInState : $.Company.RegisteredIn.State.@Name,
                                    TypeOfIncorporation : $.Company.CompanyType.@Type,
                                    ABN : $.Company.CompanyNumber.@ABN,
                                    ACN : $.Company.CompanyNumber.@ACN,
                                    ABRN : $.Company.CompanyNumber.@ABRN
		        ): {
				  Business @(Industry : $.Company.@NatureOfBusiness,
				             IndustryCode : $.Company.@ANZSIC): {},
				  Contact @(Email : $.Company.ContactDetails.Email,
				            x_MailingAddress : $.Company.ContactDetails.AddressDetails.RelatedEntityRef,
							PreferredContact : "Work" when $.Company.ContactDetails.WorkPhone.@PreferredContactMethod? otherwise "Home") : {
								ContactPerson @(NameTitle : $.Company.ContactPersonName.NameTitle.@Value,
				                                FirstName : $.Company.ContactPersonName.FirstName,
								                Surname : $.Company.ContactPersonName.Surname,
								                OtherName : $.Company.ContactPersonName.OtherName) :{},
								Mobile @(Number: $.Company.ContactDetails.WorkPhone.Phone.Mobile): {},
								(($.Company.ContactDetails.WorkPhone.*Phone default []) map {
								    (OfficePhone @(Number: $.FixedPhone,
											  AustralianDialingCode : $.FixedPhone.@AreaCode,
											  CountryCode : $.FixedPhone.@CountryCode): {}) when $.FixedPhone?,
								     (OfficeFax @(Number: $.Fax,
								                 AustralianDialingCode : $.Fax.@AreaCode,
											     CountryCode : $.Fax.@CountryCode): {}
								     )when $.Fax?											  
								})
					}  
				}) when $.Company?,
				(TrustApplicant @(UniqueID : $.Identifier ++ '-1',
 	                              Category : "Applicant",
                                  PartyType : "Trust",		
                                  Role : $.@Type,
                                  PrimaryApplicant : $.@PrimaryApplicant,
                                  IsExistingCustomer : $.Privacy.@ExistingCustomer,
                                  AllowDirectMarketing : $.Privacy.@AllowDirectMarketing,		
                                  TrustName : $.Trust.@TrustName,
								  TrustType : $.Trust.@TrustType,
								  RegisteredDate: $.Trust.@TrustDate,
								  x_Address : $.Trust.RelatedEntityRef): {
		        })when $.Trust?,
		        (PersonApplicant @(UniqueID : $.Identifier,
				                   Category : "Applicant",
								   PartyType : "Person",
                                   Role :  $.@Type,
                                   PrimaryApplicant : $.@PrimaryApplicant,
                                   IsExistingCustomer : $.Privacy.@ExistingCustomer,
								   AllowDirectMarketing : $.Privacy.@AllowDirectMarketing,
								   DateOfBirth : $.Person.DateOfBirth,
								   ResidencyStatus : $.Person.Residency.@Status,
								   MaritalStatus : $.Person.MaritalStatus.@Status,
								   Gender : $.Person.@Sex): {                  
					 Contact @(PreferredContact : $.Person.ContactDetails.HomePhone.@PreferredPhoneType when $.Person.ContactDetails.HomePhone.@PreferredContactMethod=='Yes' otherwise $.Person.ContactDetails.WorkPhone.@PreferredPhoneType,
							   Email : $.Person.ContactDetails.Email) : {
							    PersonName @(NameTitle : $.Person.PersonName.NameTitle.@Value,
					              FirstName : $.Person.PersonName.FirstName,
 								  OtherName : $.Person.PersonName.OtherName,
								  Surname : $.Person.PersonName.Surname) : {},
							   ($.Person.ContactDetails.*AddressDetails map {
							      (CurrentAddress @(x_Address : $.RelatedEntityRef when $.RelatedEntityRef? otherwise "",
								                    StartDate : $.StartAndEndDates.StartDate when $.StartAndEndDates.StartDate? otherwise "",
                                                    HousingStatus : $.@HousingStatus when $.@HousingStatus? otherwise "") : {} 
								  ) when $.@Residential=='Yes',
								  (MailingAddress @(x_Address : $.RelatedEntityRef when $.RelatedEntityRef? otherwise "",
								                    StartDate : $.StartAndEndDates.StartDate when $.StartAndEndDates.StartDate? otherwise "",
                                                    HousingStatus : $.@HousingStatus when $.@HousingStatus? otherwise "") : {} 
								  ) when $.@Mailing=='Yes',
								  (PostSettlementAddress @(x_Address : $.RelatedEntityRef when $.RelatedEntityRef? otherwise "",
								                           StartDate : $.StartAndEndDates.StartDate when $.StartAndEndDates.StartDate? otherwise "",
                                                            HousingStatus : $.@HousingStatus when $.@HousingStatus? otherwise "") : {} 
								  ) when $.@PostSettlement=='Yes',
								  (PostSettlementMailingAddress @(x_Address : $.RelatedEntityRef when  $.RelatedEntityRef? otherwise "",
									                    StartDate : $.StartAndEndDates.StartDate when  $.StartAndEndDates.StartDate? otherwise "",
	                                                    HousingStatus : $.@HousingStatus when $.@HousingStatus? otherwise "") : {} 
								   ) when $.@PostSettlementMailing=='Yes'								  
								}),
								(($.Person.ContactDetails.WorkPhone.*Phone default []) map {
								    (OfficePhone @(Number: $.FixedPhone,
											  AustralianDialingCode : $.FixedPhone.@AreaCode,
											  CountryCode : $.@CountryCode): {}) when $.FixedPhone?,
								     (OfficeFax @(Number: $.Fax,
								                 AustralianDialingCode : $.Fax.@AreaCode,
											     CountryCode : $.Fax.@CountryCode): {}											 
								     )when $.Fax?,
								     (Mobile @(Number: $.Mobile): {}) when $.Mobile? 												  
								}),
								(($.Person.ContactDetails.HomePhone.*Phone default []) map {
								    (HomePhone @(Number: $.FixedPhone,
											  AustralianDialingCode : $.FixedPhone.@AreaCode,
											  CountryCode : $.@CountryCode): {}) when $.FixedPhone?,	
                                    (Mobile @(Number: $.Mobile): {}) when $.Mobile? 											  
								})							
					}			  
		        })when $.Person?				
		  }),
		  (payload.Application.RelatedPartySegment.*RelatedParty map {
		     (RelatedCompany @(UniqueID : $.Identifier,
			                   Category : "RelatedParty",
							   PartyType : "Company",
							   Role : $.@RelPartyType,
							   BusinessName: $.CompanyName.@BusinessName,
							   ABN : $.CompanyNumber.@ABN,
							   ACN : $.CompanyNumber.@ACN,
							   ABRN : $.CompanyNumber.@ABRN,
							   DateRegistered : $.RegisteredIn.Date
							   
			 ): {
			   Contact @(x_Address : $.RelatedEntityRef,
			             Email : $.Email) : {
						       PersonName @(NameTitle : $.PersonName.NameTitle.@Value,
							                FirstName : $.PersonName.FirstName,
											OtherName : $.PersonName.OtherName,
											Surname : $.PersonName.Surname
							   ) :{},
							   (($.WorkPhone.*Phone default []) map {
								    (OfficePhone @(Number: $.FixedPhone,
											  AustralianDialingCode : $.FixedPhone.@AreaCode,
											  CountryCode : $.FixedPhone.@CountryCode): {}) when $.FixedPhone?,
								     (OfficeFax @(Number: $.Fax,
								                 AustralianDialingCode : $.Fax.@AreaCode,
											     CountryCode : $.Fax.@CountryCode): {})when $.Fax?,
                                    (Mobile @(Number: $.Mobile): {}) when $.Mobile? 												 
								}),
								(($.HomePhone.*Phone default []) map {
								    (HomePhone @(Number: $.FixedPhone,
											  AustralianDialingCode : $.FixedPhone.@AreaCode,
											  CountryCode : $.@CountryCode): {}) when $.FixedPhone?,	
                                    (Mobile @(Number: $.Mobile): {}) when $.Mobile? 											  
								})	
						 } 
			 }) when $.CompanyName?
		  }),
		  ((payload.Application.SalesChannel.*Address default []) map {
		       Address @(UniqueID : genAddId,
			             City : $.City,
						 AustralianState : $.State.@Name,
						 AustralianPostCode : $.Postcode,
						 Country : $.Country.@ISO3166) : {
						    NonStdAddress @(AddressLine1 : $.NonStdAddress) :{},
							Standard @(StreetNo : $.StreetNo,
							            Street : $.Street,
										StreetType : $.Street.@Type) : {}
						 }
		  }),
		  ((payload.Application.AddressSegment.*AddressWrapper default []) map {
		      Address @(UniqueID : $.Identifier,
			            City : $.Address.City,
						AustralianState : $.Address.State.@Name,
						AustralianPostCode : $.Address.Postcode,
						Country : $.Address.Country.@ISO3166
			   ) :{
			   				NonStdAddress @(AddressLine1 : $.Address.NonStdAddress) :{}, 
							Standard @(StreetNo : $.Address.StreetNo,
							            Street : $.Address.Street,
										StreetType : $.Address.Street.@Type) : {} 
			   }
		  }),
		  (payload.Application.PartySegment.*Party map {
		      (PartyRelation @(UniqueID : $.Identifier,
			                   RelatedUniqueID : $.Company.Director.RelatedEntityRef,
							   RelationShipType : "Director",
							   RoleType : $.Company.Director.Role.@Type) : {}) when $.Company.Director?,
		      (PartyRelation @(UniqueID : $.Identifier,
			                   RelatedUniqueID : $.Company.Shareholder.RelatedEntityRef,
							   RelationShipType : "Shareholder",
							   RoleType : $.Company.Shareholder.Role.@Type) : {}) when $.Company.Shareholder?,
		      (PartyRelation @(UniqueID : $.Identifier ++ '-1',
			                   RelatedUniqueID : $.Trust.RelatedEntityRef ,
							   RelationShipType : "Beneficiaries",
							   RoleType : $.Trust.Beneficiaries.Role.@Type) : {}) when $.Trust?,
		      (PartyRelation @(UniqueID : $.Identifier ++ '-1',
			                   RelatedUniqueID : $.Trust.BeneficialOwner.RelatedEntityRef ,
							   RelationShipType : "Beneficial Owners",
							   RoleType : $.Trust.BeneficialOwner.Role.@Type) : {}) when $.Trust.BeneficialOwner?,
		      (PartyRelation @(UniqueID : $.Identifier ++ '-1',
			                   RelatedUniqueID : $.Trust.Settlor.RelatedEntityRef ,
							   RelationShipType : "Settlor",
							   RoleType : $.Trust.Settlor.Role.@Type) : {}) when $.Trust.Settlor?							   
		  }),
		  (payload.Application.LoanDetailSegment.*LoanDetails map {
		     Summary :{
		     	 (($.*Fees default []) map {
				     Fee @(UniqueID : $.Identifier,
					        Description :  $.@Description,
							Type : $.@Type,
							Amount : $.@Amount,
							PayFeesFrom : $.@PayFeesFrom) : {}
			    })
			  }
		  }),
		 (payload.Application.LoanDetailSegment.*LoanDetails map ((i,index)->{
		        ((i.*LoanPortion default []) map {
				    LoanDetails @(UniqueID: (i.Identifier as :string) ++ '-' ++ ((ctr + 1) as :string),
					              EstimatedSettlementDate : i.EstimatedSettlement,
								  ProductName : $.@ProductName,
								  AmountRequested : $.AmountRequested.@Amount,
								  ProposedAnnualInterestRate : $.InterestRate.@RatePercent) : {
								    LoanPurpose @(PrimaryPurpose : i.LoanPurpose.@PrimaryPurpose) : {},
									LendingPurpose @(PurposeAmount: i.LoanPurpose.LendingPurposeCode.@PurposeAmount,
									                  ABSLendingPurposeCode : i.LoanPurpose.LendingPurposeCode.@ABSCode) : {},
								    (($.*LoanTerm default []) map ((j,indexj)-> {
									    (Term @(PaymentType : j.@PaymentType,
										        InterestTypeUnits : j.@Units,
												InterestTypeDuration : j.LoanTerm,
												InterestType: j.@Type,
												TotalTermDuration: $.LoanTerm) : {}) when j.@Type=='Variable'}))  					  
								  }
				}) 
		 })),
		 (payload.Application.FinancialSegment.*ValueItem map {
		     (RealEstateAsset @(UniqueID : $.Identifier,
			                    Status : $.Asset.RealEstate.@Status,
								Holding : $.Asset.RealEstate.@Holding,
								ApprovalInPrinciple : $.Asset.RealEstate.@ApprovalInPrinciple,
								PrimaryPurpose : $.Asset.RealEstate.@PropertyPrimaryPurpose,
								PrimarySecurity : $.Asset.RealEstate.@PrimarySecurity,
								Transaction : $.Asset.RealEstate.@Transaction,
								x_Address : $.Asset.RealEstate.Location.RelatedEntityRef) : {
							       (Title @(TenureType : $.Asset.RealEstate.Location.Title.@TenureType,
								           TitleType : $.Asset.RealEstate.Location.Title.@TitleType,
										   TorrensFolio : $.Asset.RealEstate.Location.Title.*Identifier[0].@Value when $.Asset.RealEstate.Location.Title is :object otherwise "",
										   TorrensVolume : $.Asset.RealEstate.Location.Title.*Identifier[1].@Value when $.Asset.RealEstate.Location.Title is :object otherwise ""
//										   (($.Asset.RealEstate.Location.Title.*Identifier default []) map {
//										   	    TorrensFolio : $.@Value when $.@Name=='TorrensFolio' otherwise "",
//										   	    TorrensVolume : $.@Value when $.@Name=='TorrensVolume' otherwise ""
//										   })
										   ) : {}) when  $.Asset.RealEstate.Location.Title?, 
								   Residential @(Type : $.Asset.RealEstate.Residential.@Type) : {},
								   EstimatedValue @(Value : $.Asset.RealEstate.EstimatedValue) : {},
								   VisitContact @(Type: $.Asset.RealEstate.VisitContact.@Type) : {},
								   Insurance @(InsuredAmount : $.Asset.RealEstate.Insurance.@InsuredAmount,
								               InsuranceType : $.Asset.RealEstate.Insurance.@Category): {},
								   OwnedByAllApplicants : "false" when $.PercentOwned.@Percent? otherwise "true",
								   (($.*PercentOwned default []) map {
								        PercentOwned @(RelatedEntityRef : $.RelatedEntityRef,
										               Percent : $.@Percent) : {} 
								   }),
                    			   PropertyFeatures @(YearBuilt : $.Asset.RealEstate.PropertyFeatures.@YearBuilt) : {}  					   
			 }) when $.Asset.RealEstate?,
			 (NonRealEstateAsset @(UniqueID : $.Identifier,
			                       ToBeUsedAsSecurity : $.Asset.NonRealEstate.@PrimarySecurity) : {			      
								   (FinancialAsset @(Description : $.Asset.NonRealEstate.OtherAsset.@OtherAssetDescription,
								                     Type : $.Asset.NonRealEstate.OtherAsset.@OtherAssetType): {
											AccountNumber @(AccountNumber : $.Asset.NonRealEstate.OtherAsset.AccountNumber.@AcctNbr,
											                BSB : $.Asset.NonRealEstate.OtherAsset.AccountNumber.@BSB) : {},
										    EstimatedValue @(Value : $.Asset.NonRealEstate.EstimatedValue) : {}
								   }) when $.Asset.NonRealEstate.OtherAsset.@OtherAssetType=='CashManagement',
								   OwnedByAllApplicants : "false" when $.PercentOwned.@Percent? otherwise "true",
								   (($.*PercentOwned default []) map {
								        PercentOwned @(RelatedEntityRef : $.RelatedEntityRef,
										               Percent : $.@Percent) : {} 
								   })
			 }) when $.Asset.NonRealEstate?,
			 (Liability @(UniqueID : $.Identifier,
			              Type : $.Liability.@Type,
						  OutstandingBalance : $.Liability.@UnpaidBalance,
						  UndrawnAmount : $.Liability.@ScheduledBalance) :{
			                   RelatedEntity @(UniqueID : $.Liability.RelatedEntityRef) : {},
							   ExistingLoan @(ClearingFromThisLoan : $.Liability.ExistingLoan.@ClearingFromThisLoan) : {},
							  OwnedByAllApplicants : "false" when $.PercentOwned.@Percent? otherwise "true",
								   (($.*PercentOwned default []) map {
								        PercentOwned @(RelatedEntityRef : $.RelatedEntityRef,
										               Percent : $.@Percent) : {} 
								   })							   
			 }) when $.Liability?
		 })
	  }
 }  
}
}